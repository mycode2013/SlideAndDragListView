# SlideAndDragListView

SlideAndDragListView，可排序、可滑动item显示”菜单”的ListView。

<img width="300" height="553" src="https://raw.githubusercontent.com/yydcdut/SlideAndDragListView/master/gif/slide.gif" /> <img width="300" height="553" src="https://raw.githubusercontent.com/yydcdut/SlideAndDragListView/master/gif/drag.gif" />

# Overview


SlideAndDragListView(SDLV)继承于Android的ListView，SDLV可以拖动item到SDLV的任意位置，其中包括了拖动item往上滑和往下滑；SDLV可以向右滑动item，像Android的QQ那样（QQ是向左滑），然后显现出来"菜单”之类的按钮。

 优秀的特性:

1. 简洁明了的拖动操作.
2. 直观流畅的滚动，拖动或滑动.
3. 支持 onItemClick 和 onItemLongClick 监听器.
4. 公开的回调事件.
5. 等等...

SlideAndDragListView is useful for all kinds of prioritized lists: favorites, playlists, checklists, etc. Would love to hear about your use case or app by email. I hope you find it useful; and please, help me improve the thing!


# Github

https://github.com/yydcdut/SlideAndDragListView

# Blog

http://www.cnblogs.com/yydcdut/p/4737552.html

# Widget Usage

## XML usage

``` xml
<com.yydcdut.sdlv.SlideAndDragListView
        xmlns:sdlv="http://schemas.android.com/apk/res-auto"
        android:layout_width="fill_parent"
        android:layout_height="fill_parent"
        android:divider="@android:color/black"
        android:dividerHeight="0.5dip"
        android:paddingLeft="8dip"
        android:paddingRight="8dip"
        sdlv:item_background="@android:color/white"
        sdlv:item_btn1_background="@drawable/btn1_drawable"
        sdlv:item_btn1_text="Delete1"
        sdlv:item_btn2_background="@drawable/btn2_drawable"
        sdlv:item_btn2_text="Rename1"
        sdlv:item_btn_number="2"
        sdlv:item_btn_text_color="#ffffff"
        sdlv:item_btn_text_size="8sp"
        sdlv:item_btn_width="70dip"
        sdlv:item_height="80dip">
</com.yydcdut.sdlv.SlideAndDragListView>
```

## XML attributes

`item_background` - item滑开那部分的背景.

`item_btn1_background` - "菜单"中第一个button的背景.

`item_btn1_text` - "菜单"中第一个button的text.

`item_btn2_background` - "菜单"中第二个button的背景.

`item_btn2_text` - "菜单"中第二个button的text.

`item_btn_number` - 要显示出来的”菜单”中的button的个数，在0~2之间.

`item_btn_width` - “菜单”中button的宽度.

`item_btn_text_color` - "菜单"中button的字体颜色.

`item_btn_text_size` - "菜单"中button的字体大小.

`item_height` - item的高度.

## Listeners

> SlideAndDragListView.OnListItemLongClickListener

``` java
sdlv.setOnListItemLongClickListener(new SlideAndDragListView.OnListItemLongClickListener() {
            @Override
            public void onListItemLongClick(View view, int position) {

            }
        });
```

`public void onListItemLongClick(View view, int position)` . 参数 view 是被长点击的item, 参数 position 是item在SDLV中的位置。.

> SlideAndDragListView.OnListItemClickListener

``` java
sdlv.setOnListItemClickListener(new SlideAndDragListView.OnListItemClickListener() {
            @Override
            public void onListItemClick(View v, int position) {

            }
        });
```

`public void onListItemClick(View view, int position)` . 参数 view 是被点击的item, 参数 position 是item在SDLV中的位置.

> SlideAndDragListView.OnDragListener

``` java
sdlv.setOnDragListener(new SlideAndDragListView.OnDragListener() {
            @Override
            public void onDragViewMoving(int position) {

            }

            @Override
            public void onDragViewDown(int position) {

            }
        });
```

`public void onDragViewMoving(int position)` .参数 position 是被拖动的item的现在所在的位置,同时onDragViewMoving这个方法会被不停的调用，因为一直在拖动，同时position也会改变.

`public void onDragViewDown(int position)` . 参数 position 是被拖动的item被放下的时候在SDLV中的位置.

> SlideAndDragListView.OnSlideListener

``` java
sdlv.setOnSlideListener(new SlideAndDragListView.OnSlideListener() {
            @Override
            public void onSlideOpen(View view, int position) {

            }

            @Override
            public void onSlideClose(View view, int position) {

            }
        });
```

`public void onSlideOpen(View view, int position)`. 参数 view 是滑动开的那个item, 同时 position 是那个item在SDLV中的位置。.

`public void onSlideClose(View view, int position)`. 参数 view 是滑动关闭的那个item, 同时 position 是那个item在SDLV中的位置.

> SlideAndDragListView.OnButtonClickListenerProxy

``` java
sdlv.setOnButtonClickListenerProxy(new SlideAndDragListView.OnButtonClickListenerProxy() {
            @Override
            public void onClick(View view, int position, int number) {

            }
        });
```

`public void onClick(View view, int position, int number)` . 参数 view 是”菜单”中被点击的button，参数 position 这个button所在的item在SDLV中的位置，参数, number 代表哪一个被点击了，因为可能会有2个.

# Permission

``` xml
<uses-permission android:name="android.permission.VIBRATE"/>
```